library serializers;

import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:demo_jsonplaceholder/api-sdk/model/album.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/comment.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/photo.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/post.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/todo.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/user.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/user_address.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/user_address_geo.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/user_company.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  Album,
  Comment,
  Photo,
  Post,
  Todo,
  User,
  UserAddress,
  UserAddressGeo,
  UserCompany,
])

//allow all models to be serialized within a list
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Album)]),
          () => new ListBuilder<Album>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Comment)]),
          () => new ListBuilder<Comment>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Photo)]),
          () => new ListBuilder<Photo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Post)]),
          () => new ListBuilder<Post>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Todo)]),
          () => new ListBuilder<Todo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(User)]),
          () => new ListBuilder<User>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserAddress)]),
          () => new ListBuilder<UserAddress>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserAddressGeo)]),
          () => new ListBuilder<UserAddressGeo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserCompany)]),
          () => new ListBuilder<UserCompany>())
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
