library openapi.api;

import 'package:dio/dio.dart';
import 'package:built_value/serializer.dart';
import 'package:demo_jsonplaceholder/api-sdk/serializers.dart';
import 'package:demo_jsonplaceholder/api-sdk/auth/api_key_auth.dart';
import 'package:demo_jsonplaceholder/api-sdk/auth/basic_auth.dart';
import 'package:demo_jsonplaceholder/api-sdk/auth/oauth.dart';
import 'package:demo_jsonplaceholder/api-sdk/api/albums_api.dart';
import 'package:demo_jsonplaceholder/api-sdk/api/comments_api.dart';
import 'package:demo_jsonplaceholder/api-sdk/api/photos_api.dart';
import 'package:demo_jsonplaceholder/api-sdk/api/posts_api.dart';
import 'package:demo_jsonplaceholder/api-sdk/api/todos_api.dart';
import 'package:demo_jsonplaceholder/api-sdk/api/users_api.dart';

final _defaultInterceptors = [
  OAuthInterceptor(),
  BasicAuthInterceptor(),
  ApiKeyAuthInterceptor()
];

class Openapi {
  Dio dio;
  Serializers serializers;
  String basePath = "https://jsonplaceholder.typicode.com";

  Openapi(
      {this.dio,
      Serializers serializers,
      String basePathOverride,
      List<Interceptor> interceptors}) {
    if (dio == null) {
      BaseOptions options = new BaseOptions(
        baseUrl: basePathOverride ?? basePath,
        connectTimeout: 5000,
        receiveTimeout: 3000,
      );
      this.dio = new Dio(options);
    }

    if (interceptors == null) {
      this.dio.interceptors.addAll(_defaultInterceptors);
    } else {
      this.dio.interceptors.addAll(interceptors);
    }

    this.serializers = serializers ?? standardSerializers;
  }

  void setOAuthToken(String name, String token) {
    (this.dio.interceptors.firstWhere((element) => element is OAuthInterceptor,
            orElse: null) as OAuthInterceptor)
        ?.tokens[name] = token;
  }

  void setBasicAuth(String name, String username, String password) {
    (this.dio.interceptors.firstWhere(
            (element) => element is BasicAuthInterceptor,
            orElse: null) as BasicAuthInterceptor)
        ?.authInfo[name] = BasicAuthInfo(username, password);
  }

  void setApiKey(String name, String apiKey) {
    (this.dio.interceptors.firstWhere(
            (element) => element is ApiKeyAuthInterceptor,
            orElse: null) as ApiKeyAuthInterceptor)
        ?.apiKeys[name] = apiKey;
  }

  /**
    * Get AlbumsApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  AlbumsApi getAlbumsApi() {
    return AlbumsApi(dio, serializers);
  }

  /**
    * Get CommentsApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  CommentsApi getCommentsApi() {
    return CommentsApi(dio, serializers);
  }

  /**
    * Get PhotosApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  PhotosApi getPhotosApi() {
    return PhotosApi(dio, serializers);
  }

  /**
    * Get PostsApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  PostsApi getPostsApi() {
    return PostsApi(dio, serializers);
  }

  /**
    * Get TodosApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  TodosApi getTodosApi() {
    return TodosApi(dio, serializers);
  }

  /**
    * Get UsersApi instance, base route and serializer can be overridden by a given but be careful,
    * by doing that all interceptors will not be executed
    */
  UsersApi getUsersApi() {
    return UsersApi(dio, serializers);
  }
}
