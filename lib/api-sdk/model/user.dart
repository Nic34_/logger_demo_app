import 'package:demo_jsonplaceholder/api-sdk/model/user_address.dart';
import 'package:demo_jsonplaceholder/api-sdk/model/user_company.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user.g.dart';

abstract class User implements Built<User, UserBuilder> {
  @nullable
  @BuiltValueField(wireName: r'id')
  int get id;

  @nullable
  @BuiltValueField(wireName: r'name')
  String get name;

  @nullable
  @BuiltValueField(wireName: r'username')
  String get username;

  @nullable
  @BuiltValueField(wireName: r'email')
  String get email;

  @nullable
  @BuiltValueField(wireName: r'phone')
  String get phone;

  @nullable
  @BuiltValueField(wireName: r'website')
  String get website;

  @nullable
  @BuiltValueField(wireName: r'company')
  UserCompany get company;

  @nullable
  @BuiltValueField(wireName: r'address')
  UserAddress get address;

  // Boilerplate code needed to wire-up generated code
  User._();

  factory User([updates(UserBuilder b)]) = _$User;
  static Serializer<User> get serializer => _$userSerializer;
}
