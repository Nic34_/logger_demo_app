// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_address_geo.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserAddressGeo> _$userAddressGeoSerializer =
    new _$UserAddressGeoSerializer();

class _$UserAddressGeoSerializer
    implements StructuredSerializer<UserAddressGeo> {
  @override
  final Iterable<Type> types = const [UserAddressGeo, _$UserAddressGeo];
  @override
  final String wireName = 'UserAddressGeo';

  @override
  Iterable<Object> serialize(Serializers serializers, UserAddressGeo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.lat != null) {
      result
        ..add('lat')
        ..add(serializers.serialize(object.lat,
            specifiedType: const FullType(String)));
    }
    if (object.lng != null) {
      result
        ..add('lng')
        ..add(serializers.serialize(object.lng,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UserAddressGeo deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserAddressGeoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'lat':
          result.lat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lng':
          result.lng = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserAddressGeo extends UserAddressGeo {
  @override
  final String lat;
  @override
  final String lng;

  factory _$UserAddressGeo([void Function(UserAddressGeoBuilder) updates]) =>
      (new UserAddressGeoBuilder()..update(updates)).build();

  _$UserAddressGeo._({this.lat, this.lng}) : super._();

  @override
  UserAddressGeo rebuild(void Function(UserAddressGeoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserAddressGeoBuilder toBuilder() =>
      new UserAddressGeoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserAddressGeo && lat == other.lat && lng == other.lng;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, lat.hashCode), lng.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserAddressGeo')
          ..add('lat', lat)
          ..add('lng', lng))
        .toString();
  }
}

class UserAddressGeoBuilder
    implements Builder<UserAddressGeo, UserAddressGeoBuilder> {
  _$UserAddressGeo _$v;

  String _lat;
  String get lat => _$this._lat;
  set lat(String lat) => _$this._lat = lat;

  String _lng;
  String get lng => _$this._lng;
  set lng(String lng) => _$this._lng = lng;

  UserAddressGeoBuilder();

  UserAddressGeoBuilder get _$this {
    if (_$v != null) {
      _lat = _$v.lat;
      _lng = _$v.lng;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserAddressGeo other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserAddressGeo;
  }

  @override
  void update(void Function(UserAddressGeoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserAddressGeo build() {
    final _$result = _$v ?? new _$UserAddressGeo._(lat: lat, lng: lng);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
