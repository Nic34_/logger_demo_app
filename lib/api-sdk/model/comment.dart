import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'comment.g.dart';

abstract class Comment implements Built<Comment, CommentBuilder> {
  @nullable
  @BuiltValueField(wireName: r'id')
  int get id;

  @nullable
  @BuiltValueField(wireName: r'postId')
  int get postId;

  @nullable
  @BuiltValueField(wireName: r'name')
  String get name;

  @nullable
  @BuiltValueField(wireName: r'email')
  String get email;

  @nullable
  @BuiltValueField(wireName: r'body')
  String get body;

  // Boilerplate code needed to wire-up generated code
  Comment._();

  factory Comment([updates(CommentBuilder b)]) = _$Comment;
  static Serializer<Comment> get serializer => _$commentSerializer;
}
