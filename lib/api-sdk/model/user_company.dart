import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_company.g.dart';

abstract class UserCompany implements Built<UserCompany, UserCompanyBuilder> {
  @nullable
  @BuiltValueField(wireName: r'name')
  String get name;

  @nullable
  @BuiltValueField(wireName: r'catchPhrase')
  String get catchPhrase;

  @nullable
  @BuiltValueField(wireName: r'bs')
  String get bs;

  // Boilerplate code needed to wire-up generated code
  UserCompany._();

  factory UserCompany([updates(UserCompanyBuilder b)]) = _$UserCompany;
  static Serializer<UserCompany> get serializer => _$userCompanySerializer;
}
