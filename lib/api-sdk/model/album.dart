import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'album.g.dart';

abstract class Album implements Built<Album, AlbumBuilder> {
  @nullable
  @BuiltValueField(wireName: r'id')
  int get id;

  @nullable
  @BuiltValueField(wireName: r'userId')
  int get userId;

  @nullable
  @BuiltValueField(wireName: r'title')
  String get title;

  // Boilerplate code needed to wire-up generated code
  Album._();

  factory Album([updates(AlbumBuilder b)]) = _$Album;
  static Serializer<Album> get serializer => _$albumSerializer;
}
