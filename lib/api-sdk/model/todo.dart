import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'todo.g.dart';

abstract class Todo implements Built<Todo, TodoBuilder> {
  @nullable
  @BuiltValueField(wireName: r'id')
  int get id;

  @nullable
  @BuiltValueField(wireName: r'userId')
  int get userId;

  @nullable
  @BuiltValueField(wireName: r'title')
  String get title;

  @nullable
  @BuiltValueField(wireName: r'completed')
  bool get completed;

  // Boilerplate code needed to wire-up generated code
  Todo._();

  factory Todo([updates(TodoBuilder b)]) = _$Todo;
  static Serializer<Todo> get serializer => _$todoSerializer;
}
