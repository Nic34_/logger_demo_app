import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'photo.g.dart';

abstract class Photo implements Built<Photo, PhotoBuilder> {
  @nullable
  @BuiltValueField(wireName: r'id')
  int get id;

  @nullable
  @BuiltValueField(wireName: r'albumId')
  int get albumId;

  @nullable
  @BuiltValueField(wireName: r'title')
  String get title;

  @nullable
  @BuiltValueField(wireName: r'url')
  String get url;

  @nullable
  @BuiltValueField(wireName: r'thumbnailUrl')
  String get thumbnailUrl;

  // Boilerplate code needed to wire-up generated code
  Photo._();

  factory Photo([updates(PhotoBuilder b)]) = _$Photo;
  static Serializer<Photo> get serializer => _$photoSerializer;
}
