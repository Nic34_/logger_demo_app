// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_address.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserAddress> _$userAddressSerializer = new _$UserAddressSerializer();

class _$UserAddressSerializer implements StructuredSerializer<UserAddress> {
  @override
  final Iterable<Type> types = const [UserAddress, _$UserAddress];
  @override
  final String wireName = 'UserAddress';

  @override
  Iterable<Object> serialize(Serializers serializers, UserAddress object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.street != null) {
      result
        ..add('street')
        ..add(serializers.serialize(object.street,
            specifiedType: const FullType(String)));
    }
    if (object.suite != null) {
      result
        ..add('suite')
        ..add(serializers.serialize(object.suite,
            specifiedType: const FullType(String)));
    }
    if (object.city != null) {
      result
        ..add('city')
        ..add(serializers.serialize(object.city,
            specifiedType: const FullType(String)));
    }
    if (object.zipcode != null) {
      result
        ..add('zipcode')
        ..add(serializers.serialize(object.zipcode,
            specifiedType: const FullType(String)));
    }
    if (object.geo != null) {
      result
        ..add('geo')
        ..add(serializers.serialize(object.geo,
            specifiedType: const FullType(UserAddressGeo)));
    }
    return result;
  }

  @override
  UserAddress deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserAddressBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'street':
          result.street = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'suite':
          result.suite = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'zipcode':
          result.zipcode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'geo':
          result.geo.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserAddressGeo)) as UserAddressGeo);
          break;
      }
    }

    return result.build();
  }
}

class _$UserAddress extends UserAddress {
  @override
  final String street;
  @override
  final String suite;
  @override
  final String city;
  @override
  final String zipcode;
  @override
  final UserAddressGeo geo;

  factory _$UserAddress([void Function(UserAddressBuilder) updates]) =>
      (new UserAddressBuilder()..update(updates)).build();

  _$UserAddress._({this.street, this.suite, this.city, this.zipcode, this.geo})
      : super._();

  @override
  UserAddress rebuild(void Function(UserAddressBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserAddressBuilder toBuilder() => new UserAddressBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserAddress &&
        street == other.street &&
        suite == other.suite &&
        city == other.city &&
        zipcode == other.zipcode &&
        geo == other.geo;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, street.hashCode), suite.hashCode), city.hashCode),
            zipcode.hashCode),
        geo.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserAddress')
          ..add('street', street)
          ..add('suite', suite)
          ..add('city', city)
          ..add('zipcode', zipcode)
          ..add('geo', geo))
        .toString();
  }
}

class UserAddressBuilder implements Builder<UserAddress, UserAddressBuilder> {
  _$UserAddress _$v;

  String _street;
  String get street => _$this._street;
  set street(String street) => _$this._street = street;

  String _suite;
  String get suite => _$this._suite;
  set suite(String suite) => _$this._suite = suite;

  String _city;
  String get city => _$this._city;
  set city(String city) => _$this._city = city;

  String _zipcode;
  String get zipcode => _$this._zipcode;
  set zipcode(String zipcode) => _$this._zipcode = zipcode;

  UserAddressGeoBuilder _geo;
  UserAddressGeoBuilder get geo => _$this._geo ??= new UserAddressGeoBuilder();
  set geo(UserAddressGeoBuilder geo) => _$this._geo = geo;

  UserAddressBuilder();

  UserAddressBuilder get _$this {
    if (_$v != null) {
      _street = _$v.street;
      _suite = _$v.suite;
      _city = _$v.city;
      _zipcode = _$v.zipcode;
      _geo = _$v.geo?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserAddress other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserAddress;
  }

  @override
  void update(void Function(UserAddressBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserAddress build() {
    _$UserAddress _$result;
    try {
      _$result = _$v ??
          new _$UserAddress._(
              street: street,
              suite: suite,
              city: city,
              zipcode: zipcode,
              geo: _geo?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'geo';
        _geo?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserAddress', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
