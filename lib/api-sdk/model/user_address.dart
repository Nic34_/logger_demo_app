import 'package:demo_jsonplaceholder/api-sdk/model/user_address_geo.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_address.g.dart';

abstract class UserAddress implements Built<UserAddress, UserAddressBuilder> {
  @nullable
  @BuiltValueField(wireName: r'street')
  String get street;

  @nullable
  @BuiltValueField(wireName: r'suite')
  String get suite;

  @nullable
  @BuiltValueField(wireName: r'city')
  String get city;

  @nullable
  @BuiltValueField(wireName: r'zipcode')
  String get zipcode;

  @nullable
  @BuiltValueField(wireName: r'geo')
  UserAddressGeo get geo;

  // Boilerplate code needed to wire-up generated code
  UserAddress._();

  factory UserAddress([updates(UserAddressBuilder b)]) = _$UserAddress;
  static Serializer<UserAddress> get serializer => _$userAddressSerializer;
}
