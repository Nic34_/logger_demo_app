import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_address_geo.g.dart';

abstract class UserAddressGeo
    implements Built<UserAddressGeo, UserAddressGeoBuilder> {
  @nullable
  @BuiltValueField(wireName: r'lat')
  String get lat;

  @nullable
  @BuiltValueField(wireName: r'lng')
  String get lng;

  // Boilerplate code needed to wire-up generated code
  UserAddressGeo._();

  factory UserAddressGeo([updates(UserAddressGeoBuilder b)]) = _$UserAddressGeo;
  static Serializer<UserAddressGeo> get serializer =>
      _$userAddressGeoSerializer;
}
