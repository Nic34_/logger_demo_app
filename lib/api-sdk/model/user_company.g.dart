// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_company.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserCompany> _$userCompanySerializer = new _$UserCompanySerializer();

class _$UserCompanySerializer implements StructuredSerializer<UserCompany> {
  @override
  final Iterable<Type> types = const [UserCompany, _$UserCompany];
  @override
  final String wireName = 'UserCompany';

  @override
  Iterable<Object> serialize(Serializers serializers, UserCompany object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.catchPhrase != null) {
      result
        ..add('catchPhrase')
        ..add(serializers.serialize(object.catchPhrase,
            specifiedType: const FullType(String)));
    }
    if (object.bs != null) {
      result
        ..add('bs')
        ..add(serializers.serialize(object.bs,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UserCompany deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserCompanyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'catchPhrase':
          result.catchPhrase = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'bs':
          result.bs = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserCompany extends UserCompany {
  @override
  final String name;
  @override
  final String catchPhrase;
  @override
  final String bs;

  factory _$UserCompany([void Function(UserCompanyBuilder) updates]) =>
      (new UserCompanyBuilder()..update(updates)).build();

  _$UserCompany._({this.name, this.catchPhrase, this.bs}) : super._();

  @override
  UserCompany rebuild(void Function(UserCompanyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserCompanyBuilder toBuilder() => new UserCompanyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserCompany &&
        name == other.name &&
        catchPhrase == other.catchPhrase &&
        bs == other.bs;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, name.hashCode), catchPhrase.hashCode), bs.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserCompany')
          ..add('name', name)
          ..add('catchPhrase', catchPhrase)
          ..add('bs', bs))
        .toString();
  }
}

class UserCompanyBuilder implements Builder<UserCompany, UserCompanyBuilder> {
  _$UserCompany _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _catchPhrase;
  String get catchPhrase => _$this._catchPhrase;
  set catchPhrase(String catchPhrase) => _$this._catchPhrase = catchPhrase;

  String _bs;
  String get bs => _$this._bs;
  set bs(String bs) => _$this._bs = bs;

  UserCompanyBuilder();

  UserCompanyBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _catchPhrase = _$v.catchPhrase;
      _bs = _$v.bs;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserCompany other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserCompany;
  }

  @override
  void update(void Function(UserCompanyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserCompany build() {
    final _$result = _$v ??
        new _$UserCompany._(name: name, catchPhrase: catchPhrase, bs: bs);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
