part of app;

class ItemView extends StatefulWidget {
  @override
  _ItemViewState createState() => _ItemViewState();
}

class _ItemViewState extends State<ItemView> {
  Post item;

  _load() async {
    final args = ModalRoute.of(context).settings.arguments;
    var res = await AppApi().api.getPostsApi().postsIdGet(args);
    setState(() {
      item = res.data;
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _load();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(
        child: (item != null) ? Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("${item.id}"),
              SizedBox(height: 10),
              Text("${item.title}", style: TextStyle(fontSize: 16)),
              SizedBox(height: 10),
              Text("${item.body}"),
            ],
          ),
        ) : Center(child: CircularProgressIndicator()) ,
      ),
    );
  }
}