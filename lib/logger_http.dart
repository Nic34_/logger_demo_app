import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoggerHttp {
  static final LoggerHttp _singleton = LoggerHttp._internal();
  String path;

  factory LoggerHttp() {
    return _singleton;
  }

  LoggerHttp._internal();

  int sessionId = 0;
  int countRequest = 0;
  final HttpClient client = new HttpClient();

  String _pathDeviceInfo = "";
  String _pathDeviceLog = "";

  init(String path) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    sessionId = prefs.getInt('session_number') ?? sessionId;
    sessionId++;
    prefs.setInt('sessionId', sessionId);

    this.path = path;
    var info = (await getDeviceDetails());
    _pathDeviceInfo = "$path/device";
    _pathDeviceLog = "$path/log";

    try {
      HttpClientRequest requestPostDevice = await HttpClient().postUrl(Uri.parse(_pathDeviceInfo))
        ..headers.contentType = ContentType.json
        ..write(jsonEncode(info));

      HttpClientResponse response = await requestPostDevice.close();
      String answer = await response.transform(utf8.decoder).join();

      sessionId = jsonDecode(answer)['session_id'];
    } catch (e) {
      print(e);
    }
  }

  preSend({
    @required RequestOptions options,
    @required String createdAt,
  }) async {
    this.countRequest++;

    try {
      Map jsonData = {
        'number': countRequest,
        'sessionId': sessionId,
        'url': options.uri.toString(),
        'code': null,
        "status": "pending",
        "method": options.method,
        'headers': options.headers,
        'headersResponse': {},
        'params': options.data,
        'response': null,
        'action': 'getElementById',
        'createdAt': createdAt,
        'responseAt': null,
        'size': null,
        'payload': null,
      };

      HttpClientRequest request = await HttpClient().postUrl(Uri.parse(_pathDeviceLog)) /*1*/
        ..headers.contentType = ContentType.json /*2*/
        ..write(jsonEncode(jsonData)); /*3*/
      request.close(); /*4*/
    } catch (e) {
      print(e);
    }

    return this.countRequest;
  }

  send({
    @required int number,
    @required Response response,
    @required String createdAt,
    @required String responseAt,
    status = 'done',
  }) async {
    try {
      Map jsonData = {
        'number': number,
        'sessionId': sessionId,
        'url': response.request.uri.toString(),
        'code': response.statusCode,
        'method': response.request.method,
        "status": status,
        'headers': response.request.headers,
        'headersResponse': response.headers.map,
        'params': response.request.data,
        'payload': response.data,
        'action': 'getElementById',
        'createdAt': createdAt,
        'responseAt': responseAt,
        'size': response.data.toString().length,
      };

      HttpClientRequest request = await HttpClient().postUrl(Uri.parse(_pathDeviceLog)) /*1*/
        ..headers.contentType = ContentType.json /*2*/
        ..write(jsonEncode(jsonData)); /*3*/
      HttpClientResponse response1 = await request.close(); /*4*/
      String answer = await response1.transform(utf8.decoder).join();

      print(jsonDecode(answer));
    } catch (e) {
      print(e);
    }
  }
}

class DeviceInfo {
  final String uuid;
  final String deviceName;
  final String deviceVersion;
  final String identifier;
  final String product;

  DeviceInfo(this.uuid, this.deviceName, this.deviceVersion, this.identifier, this.product);

  @override
  String toString() {
    return json.encode(toJson());
  }

  toJson() {
    return {
      'uuid': uuid,
      'device_name': deviceName,
      'device_version': deviceVersion,
      'identifier': identifier,
      'product': product,
    };
  }
}

Future<DeviceInfo> getDeviceDetails() async {
  final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  var res;

  try {
    if (Platform.isAndroid) {
      var build = await deviceInfoPlugin.androidInfo;
      res = DeviceInfo(
        build.androidId,
        build.model,
        build.version.toString(),
        build.androidId, //UUID for Androi,
        build.product,
      );
    } else if (Platform.isIOS) {
      var data = await deviceInfoPlugin.iosInfo;
      res = DeviceInfo(
        data.identifierForVendor,
        data.name,
        data.systemVersion,
        data.identifierForVendor, //UUID for iOS,
        data.systemName,
      );
    }
  } on PlatformException {
    print('Failed to get platform version');
  }

  return res;
}