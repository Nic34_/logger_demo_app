part of app;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Post> items = [];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _load();
  }

  _load() async {
    var res = await AppApi().api.getPostsApi().getPosts();
    setState(() {
      items = res.data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              onRefresh: () => _load(),
              child: ListView.builder(
                  physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  itemCount: items.length,
                  itemBuilder: (ctx, index) {
                    final item = items[index];
                    return Card(
                      child: ListTile(
                        onTap: () {
                          Navigator.pushNamed(context, '/view', arguments: item.id);
                        },
                        title: Text(item.title),
                      ),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
