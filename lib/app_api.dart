import 'dart:convert';

import 'package:demo_jsonplaceholder/logger_interceptor.dart';
import 'package:demo_jsonplaceholder/logger_http.dart';

import 'api-sdk/api.dart';
import 'api-sdk/api.dart';

import 'package:dio/dio.dart';

import 'api-sdk/api.dart';

/// Creates instance of [Dio] to be used in the remote layer of the app.
Dio createDio(BaseOptions baseConfiguration) {
  var dio = Dio(baseConfiguration);
  dio.interceptors.addAll([
    // interceptor to retry failed requests
    // interceptor to add bearer token to requests
    // interceptor to refresh access tokens
    // interceptor to log requests/responses
    // etc.
  ]);

  return dio;
}

/// Creates Dio Options for initializing a Dio client.
///
/// [baseUrl] Base url for the configuration
/// [connectionTimeout] Timeout when sending data
/// [connectionReadTimeout] Timeout when receiving data
BaseOptions createDioOptions(
    String baseUrl, int connectionTimeout, int connectionReadTimeout) {
  return BaseOptions(
    baseUrl: baseUrl,
    connectTimeout: connectionTimeout,
    receiveTimeout: connectionReadTimeout,
  );
}

const logger_url = "ws://10.0.2.2:6001";

/// Creates an instance of the backend API with default options.
Openapi createMyApi() {
  const baseUrl = 'https://jsonplaceholder.typicode.com';
  final options = createDioOptions(baseUrl, 10000, 10000);
  final dio = createDio(options);
  return Openapi(dio: dio, interceptors: [LoggerInterceptor(logger_url, 'json_placeholder')]);
}

class AppApi {
  static final AppApi _singleton = AppApi._internal();

  factory AppApi() {
    return _singleton;
  }

  AppApi._internal();

  Openapi api = createMyApi();
}
