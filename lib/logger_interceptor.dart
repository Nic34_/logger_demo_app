import 'dart:developer';
import 'dart:convert';

import 'package:demo_jsonplaceholder/logger_http.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io/socket_io.dart';
import 'package:web_socket_channel/io.dart';

class LoggerInterceptor extends Interceptor {
  final String loggerUrl;
  final String project;
  var countRequest = 0;
  int sessionId = 0;
  var deviceIdentifier;

  IOWebSocketChannel channel;

  LoggerInterceptor(this.loggerUrl, this.project) {
    print('[LoggerInterceptor] init');
    channel = IOWebSocketChannel.connect(this.loggerUrl);

    channel.stream.listen((message) {
      print(message);
      // channel.sink.close(status.goingAway);
    });

    getDeviceDetails().then((value) {
      deviceIdentifier = value.identifier;
      channel.sink.add(jsonEncode({
        'action': 'device_connect',
        'payload': value,
      }));
    });


    init();
  }

  init() async {
    var prefs = await SharedPreferences.getInstance();
    sessionId = prefs.getInt('session_number') ?? sessionId;
    sessionId++;
    prefs.setInt('sessionId', sessionId);


  }

  @override
  Future onRequest(RequestOptions options) async {
    if (sessionId == 0) await init();

    final createdAt = DateTime.now().toIso8601String();

    options.extra.addAll({
      'number': countRequest.toString(),
      'createdAt': createdAt,
    });

    try {
      Map jsonData = {
        'action': 'device_request',
        'payload': {
          'device_identifier': deviceIdentifier,
          'project': project,
          'number': countRequest,
          'session_id': sessionId,
          'url': options.uri.toString(),
          'code': null,
          "status": "pending",
          "method": options.method,
          'headers': options.headers,
          'headers_response': {},
          'params': options.data,
          'response': null,
          'action': 'getElementById',
          'created_at': createdAt,
          'response_at': null,
          'size': null,
          'payload': null,
        }
      };

      channel.sink.add(jsonEncode(jsonData));

    } catch (e) {
      print(e);
    }

    this.countRequest++;
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) {
    try {
      final responseAt = DateTime.now().toIso8601String();

      var number = int.parse(response.request?.extra['number'] ?? '0');
      var createdAt = response.request?.extra['createdAt'] ?? '';

      Map jsonData = {
        'action': 'device_request',
        'payload': {
          'device_identifier': deviceIdentifier,
          'project': project,
          'number': number,
          'session_id': sessionId,
          'url': response.request.uri.toString(),
          'code': response.statusCode,
          'method': response.request.method,
          "status": 'done',
          'headers': response.request.headers,
          'headers_response': response.headers.map,
          'params': response.request.data,
          'payload': response.data,
          'action': 'getElementById',
          'created_at': createdAt,
          'response_at': responseAt,
          'size': response.data
              .toString()
              .length,
        }
      };
      channel.sink.add(jsonEncode(jsonData));
    } catch (e) {
      print(e);
    }

    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) {
    var response = err.response;
    try {
      final responseAt = DateTime.now().toIso8601String();

      var number = int.parse(response.request?.extra['number'] ?? '0');
      var createdAt = response.request?.extra['createdAt'] ?? '';

      Map jsonData = {
        'action': 'device_request',
        'payload': {
          'device_identifier': deviceIdentifier,
          'project': project,
          'number': number,
          'session_id': sessionId,
          'url': response.request.uri.toString(),
          'code': response.statusCode,
          'method': response.request.method,
          "status": 'error',
          'headers': response.request.headers,
          'headers response': response.headers.map,
          'params': response.request.data,
          'payload': response.data,
          'action': 'getElementById',
          'created_at': createdAt,
          'response_at': responseAt,
          'size': response.data
              .toString()
              .length,
        }
      };
      channel.sink.add(jsonEncode(jsonData));
    } catch (e) {
      print(e);
    }

    return super.onError(err);
  }

}
