library app;

import 'package:demo_jsonplaceholder/api-sdk/model/post.dart';
import 'package:demo_jsonplaceholder/app_api.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

part 'item_view.dart';
part 'my_home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      routes: {
        '/view': (ctx) => ItemView(),
      },
    );
  }
}

